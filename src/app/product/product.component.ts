import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product'

@Component({
  selector: 'bit-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() editEvent = new EventEmitter<Product>();
  product:Product;
  tempProduct:Product = {cost:null,pname:null,category:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }
  
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }

  cancelEdit(){
    this.isEdit = false;
    this.product.pname = this.tempProduct.pname;
    this.product.cost = this.tempProduct.cost;
    this.product.category = this.tempProduct.category;
    this.editButtonText = 'Edit'; 
  }
  
  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
    
     if(this.isEdit){
       this.tempProduct.pname = this.product.pname;
       this.tempProduct.cost = this.product.cost;
       this.tempProduct.category = this.product.category;
     } else {     
       this.editEvent.emit(this.product);
     }
  }

  ngOnInit() {
  }

}
