import { Component, OnInit } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'bit-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  //conection to the father
  inputs:['post']
})
export class PostComponent implements OnInit {

  //post;
  //Changed when added class post
  post:Post;

  constructor() { }

  ngOnInit() {
  }

}
