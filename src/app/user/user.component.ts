import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user'

@Component({
  selector: 'bit-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  
  //user;
  //Changed when added class user
  //EventEmitter needed for the CRUD 
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User[]>();

  user:User;
  //new variable for clicking on eddit button 
  isEdit : boolean = false;
  editButtonText = 'Edit';
  //temporery save if we cancel the edit
  tempUser:User = {email:null,name:null};
  
  constructor() { }

  sendDelete(){
    //send to father Users
    this.deleteEvent.emit(this.user);
  }

  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     // ? means if : means else
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';  
     if(this.isEdit){
       this.tempUser = {email:this.user.email,name:this.user.name};
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempUser,this.user);
       this.editEvent.emit(originalAndNew);
     }  
  }

  cancelEdit(){
    this.isEdit = false;
    this.user.email = this.tempUser.email;
    this.user.name = this.tempUser.name;
    this.editButtonText = 'Edit'; 
  }

  ngOnInit() {
  }

}
