import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'bit-posts',
  templateUrl: './posts.component.html', 
  //insted we are adding another style below
  //styleUrls: ['./posts.component.css']
  styles: [`
    table, td, th { border: 1px solid black; }
    table { width: 100%; }
    th { height: 50px; }
    .posts tr { cursor: default; }
    .posts tr:hover { background: #ecf0f1; }
        .table-group-item.active, 
        .table-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    } 
  `]
})
export class PostsComponent implements OnInit {

  //adding posts with json code- moved to posts.service.ts when added service
  /*posts = [
    {title:'title 1',body:'this is the body of 1'},
    {title:'title 2',body:'this is the body of 2'},
    {title:'title 3',body:'this is the body of 3'}
  ]*/

  //to mark the second post(the numbering starts from 0)
  //currentPost = this.posts[1];

  posts;
  currentPost;
  isLoading = true;

  select(post){
      //selecting current user
  		this.currentPost = post; 
      console.log(	this.currentPost);
   }

   //conects to the service
   constructor(private _usersService: PostsService) {
      //removed when added date drom server
      //this.posts = this._usersService.getPosts();
   }
  //constructor() { }

  ngOnInit() {
    //get data from data server
    this._usersService.getPosts()
			    .subscribe(posts => {this.posts = posts;
                               this.isLoading = false});
  }

}
