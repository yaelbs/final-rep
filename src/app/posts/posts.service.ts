import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {

  //removed when added data server
  /*posts = [
    {title:'title 1',body:'this is the body of 1'},
    {title:'title 2',body:'this is the body of 2'},
    {title:'title 3',body:'this is the body of 3'}
  ]*/

  private _url = "http://jsonplaceholder.typicode.com/posts";
  
  //changed when added data server
  constructor(private _http: Http) { }

  //changed when added data server
  /*getPosts(){
		return this.posts;
	}*/
  //constructor() { }

  getPosts(){
		return this._http.get(this._url)
			.map(res => res.json()).delay(2000)
 	}
}
