import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'bit-users',
  templateUrl: './users.component.html',
  //insted we are adding another style below
  //styleUrls: ['./users.component.css']
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    } 
  `]
})
export class UsersComponent implements OnInit {

  //adding users with json code- moved to users.service.ts when added service
  /*users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]*/

  //to mark the second user(the numbering starts from 0)
  //currentUser = this.users[1];

  users;
  currentUser;
  isLoading = true;

  select(user){
      //selecting current user
  		this.currentUser = user; 
      //console.log(	this.currentUser);
   }

   deleteUser(user){
     //splice users in tha array plase indexOf(user) and delet only one user
    this.users.splice(
      this.users.indexOf(user),1
    )
  }

  editUser(originalAndEdited){
    this.users.splice(
      this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.users);
  }

   //conects to the service
   constructor(private _usersService: UsersService) {
      //removed when edded deta server
      //this.users = this._usersService.getUsers();
   } 
  //constructor() { }

  ngOnInit() {
    //added when added data server
    this._usersService.getUsers()
			    .subscribe(users => {this.users = users;
                               this.isLoading = false});
  }

}
