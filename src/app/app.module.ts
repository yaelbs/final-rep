import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//import for Router
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { UsersService } from './users/users.service';
import { PostsService } from './posts/posts.service';
import { ProductsService } from './products/products.service';
import { UserComponent } from './user/user.component';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { AngularFireModule } from 'angularfire2';


export const firebaseConfig = {
    apiKey: "AIzaSyAV7hXVFWgmXb4luHxYXBUDSPokpfCRpM0",
    authDomain: "example-test-ae455.firebaseapp.com",
    databaseURL: "https://example-test-ae455.firebaseio.com",
    storageBucket: "example-test-ae455.appspot.com",
    messagingSenderId: "829164391380"
}


//import for Router
const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'products', component: ProductsComponent },
  { path: '', component: ProductsComponent },
  //if none of the above go to page not found
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    ProductsComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //imports for router
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, PostsService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
