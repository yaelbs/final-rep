import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {

  productsObservable;

  constructor(private af:AngularFire, private _http:Http) { }

  addProduct(product){
    this.productsObservable.push(product);
  }

  deleteProduct(product){
    this.af.database.object('/products/' + product.$key).remove();
    console.log('/products/' + product.$key);
  }

  updateProduct(product){
    let product1 = {pname:product.pname,cost:product.cost}
    console.log(product1);
    this.af.database.object('/products/' + product.$key).update(product1)
  }  

  getProduct(){
    /*this.productsObservable = this.af.database.list('/products');                     
    return this.productsObservable; */
    this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.productCat = [];
            for(var p in product.categoryId){
                product.productCat.push(
                this.af.database.object('/categories/' + p)
              )
            }
          }
        );
        return products;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.productsObservable;
	}


  /*getUserFromRestApi(){
    let url = this._url;
    return this._http.get(url).map(res => res.json());
  }*/

}
